/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * Robots are used to repair the ship hull.
 */
public class Robots extends Upgrade {

    /**
     * How many hull points can be repaired each turn?
     */
    Integer hullRepair;

    public Robots(Integer hullRepair) {
        super(hullRepair * 100, hullRepair);
        this.hullRepair = hullRepair;
    }

    @Override
    public void reload(Ship ship) {
        if(!canUse(ship)) {
            return;
        }
        if (ship.getHull() < ship.getMaxHull()) {
            System.out.println(ship.getOwner() + " uses " + this);
            ship.repairHull(hullRepair);
        }
    }

    @Override
    public String toString() {
        return "robots (hull: " + hullRepair + ")";
    }

}
