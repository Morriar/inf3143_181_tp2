/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

import java.util.ArrayList;
import java.util.List;

/**
 * A Ship can be upgrades with items.
 */
public class Ship implements Item {

    /**
     * Ship's owner.
     */
    protected Player owner;

    /**
     * Ship's cost in credits.
     */
    protected Integer cost;

    /**
     * Maximum hull value this ship can have.
     */
    protected Integer maxHull;

    /**
     * Current hull value this ship has.
     *
     * When the hull reaches 0, the ship is destroyed.
     */
    protected Integer hull;

    /**
     * Maximum energy value this ship can have.
     */
    protected Integer maxEnergy;

    /**
     * Current energy value this ship has.
     *
     * Energy is used to power upgrades.
     */
    protected Integer energy;

    /**
     * Max number of upgrades this ship can equip.
     */
    protected Integer upgradeSlots;

    /**
     * List of upgrades equipped by this ship.
     */
    protected List<Upgrade> upgrades = new ArrayList<>();

    public Ship(Integer cost, Integer maxHull, Integer maxEnergy, Integer upgradeSlots) {
        this.cost = cost;

        this.maxHull = maxHull;
        this.hull = maxHull;

        this.maxEnergy = maxEnergy;
        this.energy = maxEnergy;

        this.upgradeSlots = upgradeSlots;
    }

    /**
     * Equip an upgrade on this ship.
     *
     * @param upgrade the upgrade to equip
     * @throws NotEnoughUpgradeSlotsException if not enough slots are available
     * for the upgrade to be equipped.
     */
    public void equip(Upgrade upgrade) throws NotEnoughUpgradeSlotsException {
        if (upgrades.size() >= upgradeSlots) {
            throw new NotEnoughUpgradeSlotsException();
        }
        upgrades.add(upgrade);
    }

    /**
     * Is this ship destroyed?
     *
     * @return true if the ship's hull reached 0.
     */
    public Boolean isDestroyed() {
        return hull <= 0;
    }

    /**
     * Remove hull points to the ship.
     *
     * @param damages the hull points to remove.
     */
    public void damageHull(Integer damages) {
        this.hull -= damages;
        if(this.hull < 0) {
            this.hull = 0;
        }
    }

    /**
     * Add hull points from the ship.
     *
     * @param hull the hull points to add.
     */
    public void repairHull(Integer hull) {
        this.hull += hull;
        if (this.hull > maxHull) {
            this.hull = maxHull;
        }
    }

    /**
     * Remove energy points from the ship.
     *
     * @param energy the energy points to remove.
     */
    public void consumeEnergy(Integer energy) {
        this.energy -= energy;
        if(this.energy < 0) {
            this.energy = 0;
        }
    }

    /**
     * Add energy points to the ship.
     *
     * @param energy the energy points to add.
     */
    public void addEnergy(Integer energy) {
        this.energy += energy;
        if (this.energy > maxEnergy) {
            this.energy = maxEnergy;
        }
    }

    /**
     * Reload all upgrades equipped by the ship.
     *
     * This method will update the cool down values of Blaster and Shield and
     * apply the gains from Robots and SolarPanels.
     */
    public void reload() {
        for (Upgrade upgrade : upgrades) {
            upgrade.reload(this);
        }
    }

    public Integer getUpgradeSlots() {
        return upgradeSlots;
    }

    public void setUpgradeSlots(Integer upgradeSlots) {
        this.upgradeSlots = upgradeSlots;
    }
    
    @Override
    public Integer getCreditsCost() {
        return cost;
    }

    public void setCreditsCost(Integer cost) {
        this.cost = cost;
    }

    public void setHull(Integer hull) {
        this.hull = hull;
    }
    
    public Integer getHull() {
        return hull;
    }

    public Integer getMaxHull() {
        return maxHull;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }
    
    public Integer getEnergy() {
        return energy;
    }

    public Integer getMaxEnergy() {
        return maxEnergy;
    }

    public List<Upgrade> getUpgrades() {
        return upgrades;
    }
    
    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public Player getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return owner.getName() + "'s ship";
    }

    public String status() {
        return "hull: " + hull + ", energy: " + energy;
    }

}
