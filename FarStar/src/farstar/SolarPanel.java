/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package farstar;

/**
 * Solar panels are used to replenish the energy of a Ship.
 */
public class SolarPanel extends Upgrade {

    /**
     * How many energy points are gained each turn?
     */
    Integer energyBoost;

    public SolarPanel(Integer energyBoost) {
        super(energyBoost * 10, 0);
        this.energyBoost = energyBoost;
    }

    @Override
    public void reload(Ship ship) {
        if(!canUse(ship)) {
            return;
        }
        if (ship.getEnergy() < ship.getMaxEnergy()) {
            System.out.println(ship.getOwner() + " uses " + this);
            ship.addEnergy(energyBoost);
        }
    }

    @Override
    public String toString() {
        return "solar panel (energy: " + energyBoost + ")";
    }

}
