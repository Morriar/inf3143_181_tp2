/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package contrats;

import farstar.NotEnoughCreditsException;
import farstar.NotEnoughUpgradeSlotsException;
import farstar.Phaser;
import farstar.Player;
import farstar.Ship;
import farstar.Upgrade;

public class TestShip15 {

    public static void main(String[] args) throws NotEnoughUpgradeSlotsException, NotEnoughCreditsException {
        Player player = new Player("p1", 100000);
        Ship ship = new Ship15(1000, 100, 100, 1);
        player.buy(ship);
        player.setShip(ship);
        Phaser phaser = new Phaser(10);
        player.buy(phaser);
        ship.equip(phaser);
    }
}

class Ship15 extends Ship {

    public Ship15(Integer cost, Integer maxHull, Integer maxEnergy, Integer upgradeSlots) {
        super(cost, maxHull, maxEnergy, upgradeSlots);
    }

    @Override
    public void equip(Upgrade upgrade) {
        this.upgrades.add(new Phaser(10));
    }
}
